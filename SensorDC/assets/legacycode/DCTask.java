package com.sensordc;

import java.io.File;
import java.util.TimerTask;

import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.text.format.Time;
import android.util.Log;
import ch.qos.logback.classic.Logger;

//Data collection task
	class DCTask extends TimerTask {
		SensorDCManager sensordcmanager;
		Context context;
		public static String TAG = "sensordctask";
		String datalogdir;
		LocationManager locationmanager;
		
		public DCTask(Context context, SensorDCManager sensordcmanager, LocationManager lm, String datalogdir) {
			this.sensordcmanager = sensordcmanager;
			this.context=context;
			this.datalogdir=datalogdir;
			this.locationmanager=lm;
		}

		//@Override
		public void run() {

			setupDirectories();
			turnGPSOn(context.getApplicationContext());

			try {
				Logger l = (Logger) LoggerFactory.getLogger(this.getClass());
				
				//sensordcmanager.Initialize();
				
				for (int i=1 ; i <=10; i++)
				{
					
					l.info(sensordcmanager.getData().toString());
					Thread.sleep(1000);
				}
				
				//sensordcmanager.Uninitialize();
				
				Time today = new Time(Time.getCurrentTimezone());
				today.setToNow();
				if(today.hour==2 && today.minute==0 && today.second<30)
				{
					rebootPhone();
				}

			} catch (Exception e) {
				SensorDCLog.e(TAG, "run " + e + " " + e.getMessage(),
						this.getClass());
			}

		}

		private void setupDirectories() {
			try {
				File dir = new File(datalogdir);
				dir.mkdirs();
			} catch (Exception e) {
				SensorDCLog.e(TAG, "setting up directories " + e,
						this.getClass());
			}

		}

		private void turnGPSOn(Context context) {
			try {
				boolean gps = locationmanager
						.isProviderEnabled(LocationManager.GPS_PROVIDER);
				if (!gps) {
					Intent intent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
					
					// Log.w(TAG, "gps radio was off ", this.getClass());
				}

				// Intent intent = new Intent(
				// "android.location.GPS_ENABLED_CHANGE");
				// intent.putExtra("enabled", true);
				// context.sendBroadcast(intent);
				//
				// String provider = Settings.Secure.getString(
				// context.getContentResolver(),
				// Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
				// if (!provider.contains("gps")) { // if gps is disabled
				// final Intent poke = new Intent();
				// poke.setClassName("com.android.settings",
				// "com.android.settings.widget.SettingsAppWidgetProvider");
				// poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
				// poke.setData(Uri.parse("3"));
				// context.sendBroadcast(poke);
				// }
			} catch (Exception e) {
				SensorDCLog.e(TAG, "turnonGPS " + e, this.getClass());
			}

		}
	
	
		private boolean rebootPhone()
		{
			try 
			{
				final String command = "reboot";
				Process proc = Runtime.getRuntime().exec(
						new String[] { "su", "-c", command });
				proc.waitFor();
				return true;
			}
			catch(Exception e)
			{
				Log.e(TAG, "reboot " + e);
				return false;
			}
		}
	
	
	}
