package com.sensordc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import android.content.Context;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

public // Data upload task
class UploadTask extends TimerTask {
	private String localdir;
	private String remotedir;
	private String remoteuser;
	private String remotehost;
	private int remoteport;
	private String remotepassword;
	Context context;
	public static String TAG = "sensordcuploadtask";

	public UploadTask(Context context, String localdir, String remotedir,
			String remotehost, String remoteuser, String password,
			int remoteport) {
		this.context = context;
		this.localdir = localdir;
		this.remotedir = remotedir;
		this.remoteuser = remoteuser;
		this.remotehost = remotehost;
		this.remoteport = remoteport;
		this.remotepassword = password;
	}

	@Override
	public void run() {

		//configureWifi();
		try {
			JSch ssh = new JSch();
			JSch.setConfig("StrictHostKeyChecking", "no");
			Session session = ssh.getSession(remoteuser, remotehost,
					remoteport);
			session.setPassword(remotepassword);

			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftp = (ChannelSftp) channel;

			// make directory remote if it does not exist
			this.mkdir(sftp, remotedir);

			// list files in remote directory
			List<String> lsr = new ArrayList<String>();
			List<LsEntry> ls_remote = sftp.ls(remotedir);
			for (LsEntry l : ls_remote) {
				lsr.add(l.getFilename());
			}
			
			
			channel.disconnect();
			session.disconnect();
			

			List<File> ls_local = ListDir(localdir);
			List<File> toUpload = new ArrayList<File>();

			// sync last log file regardless
			toUpload.add(new File(this.localdir + File.separator
					+ SensorDCService.currentdatalogfilename));

			for (File f : ls_local) {
				if (!lsr.contains(f.getName())) {

					toUpload.add(f);
				}
			}

			
			session = ssh.getSession(remoteuser, remotehost,
					remoteport);
			session.setPassword(remotepassword);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			sftp = (ChannelSftp) channel;
			
			
			for (File f : toUpload) {
				sftp.put(f.getAbsolutePath(), remotedir + "/" + f.getName());
			}

			channel.disconnect();
			session.disconnect();
		} catch (SftpException e) {
			SensorDCLog.e(TAG, "UploadTask sftp exception " + e,
					this.getClass());
		} catch (Exception e) {
			SensorDCLog.e(TAG, "UploadTask " + e, this.getClass());
		}
	}

	private List<File> ListDir(String path) {
		List<File> retVal = new ArrayList<File>();
		File f = new File(path);
		File[] files = f.listFiles();
		for (File file : files) {
			retVal.add(file);
		}
		return retVal;
	}

	// create directory if it doesnt exist
	private void mkdir(ChannelSftp sftp, String dir) {
		SftpATTRS attrs = null;
		try {
			String currentDirectory = sftp.pwd();
			attrs = sftp.stat(currentDirectory + "/" + dir);
		} catch (Exception e) {
			// do nothing
		}

		if (attrs != null) {
			return;
		} else {

			try {
				sftp.mkdir(dir);
			} catch (SftpException e) {
				SensorDCLog.e(TAG, " UploadTask mkdir " + e,
						this.getClass());
			}
		}

	}

//	private void configureWifi() {
//		try {
//			WifiManager wManager = (WifiManager) context
//					.getSystemService(Context.WIFI_SERVICE);
//			wManager.setWifiEnabled(true);
//			
//		} catch (Exception e) {
//			SensorDCLog.e("turnWifiOn ", " " + e, this.getClass());
//		}
//	}

}
