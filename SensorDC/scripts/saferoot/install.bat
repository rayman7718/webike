@echo off
cd "%~dp0"
set PATH=%SystemRoot%\system32;%PATH%
echo ---        Samsung Galaxy S4 Android 4.3 Root         ---
echo --- Based on the CVE-2013-6282 exploit by cubeundcube ---
echo.
:getbb
set /p choice="Do you want to install Busybox on your device [Y/N]? "

if %choice%==Y (
    set BB=Y
) else if %choice%==y (
    set BB=Y
) else if %choice%==N (
    set BB=N
) else if %choice%==n (
    set BB=N
) else (
    echo Please enter Y or N.
    goto getbb
)

if not exist files (
    echo [*] Fixing the folder structure
    mkdir files
    if exist adb.exe move adb.exe files
    if exist AdbWinApi.dll move AdbWinApi.dll files
    if exist AdbWinUsbApi.dll move AdbWinUsbApi.dll files
    if exist busybox move busybox files
    if exist unroot move unroot files
    if exist getroot move getroot files
    if exist install-recovery.sh move install-recovery.sh files
    if exist su move su files
    if exist Superuser.apk move Superuser.apk files
    echo.
)
echo [*] Testing adb usability
echo.
echo | set /p dummy="Plug in your phone and "
pause
for /f "delims=" %%a in ('files\adb get-state ^| findstr /v "foobar"') do @set pstate=%%a
if not %pstate%==device (
    echo.
    echo Watch your phone. If you see the "Allow USB debugging?" prompt
    echo Tap on the "Always allow from this computer" checkbox
    echo then tap OK
    echo.
    echo If this script appears to be stuck at "Waiting for your phone to appear",
    echo then you should tap on the USB icon in the notifications that says
    echo "Connected as a media device". On the"USB computer connection"
    echo page, switch between "Camera" and "Media Device" to see if the
    echo device appears.
)
echo.
echo [*] Waiting for your phone to appear
files\adb wait-for-device
echo [*] Your phone is detected and ready for rooting.
echo.
echo [*] Sending files to your device...
files\adb install files\Superuser.apk >NUL 2>&1
files\adb push files\getroot /data/local/tmp/ >NUL 2>&1
files\adb push files\su /data/local/tmp/ >NUL 2>&1
if %BB%==Y files\adb push files\busybox /data/local/tmp/ >NUL 2>&1
files\adb push files\install-recovery.sh /data/local/tmp/ >NUL 2>&1
files\adb push files\unroot /data/local/tmp/ >NUL 2>&1
files\adb shell chmod 0755 /data/local/tmp/getroot
if %BB%==Y files\adb shell chmod 0755 /data/local/tmp/busybox
echo.
::echo [*] Displaying initial status
::files\adb shell ls -l /system/xbin/su /system/xbin/daemonsu
::echo.
echo [*] Starting rooting program.
if %BB%==Y (
    files\adb shell /data/local/tmp/getroot
) else (
    files\adb shell /data/local/tmp/getroot -b
)
echo.
echo [*] Checking if rooting succeeded
setlocal
for /f "tokens=*" %%a in ('files\adb shell ls -l /data/local/tmp/su ^| findstr /C:"No such file or directory"') do @set rootedState=%%a
if "%rootedState%" == "" (
    endlocal
    echo.
    echo [*] Unfortunately, rooting failed. Cleaning up.
    files\adb shell rm /data/local/tmp/getroot
    files\adb shell rm /data/local/tmp/su
    if %BB%==Y files\adb shell rm /data/local/tmp/busybox
    files\adb shell rm /data/local/tmp/install-recovery.sh
    files\adb shell rm /data/local/tmp/unroot
    goto exit
)
echo [*] Removing temporary files...
files\adb shell rm /data/local/tmp/getroot
echo.
echo [*] Trying to disable Knox...
files\adb shell /system/xbin/super -c pm disable com.sec.knox.seandroid
echo.
echo [*] Rebooting... Please wait.
files\adb reboot
echo [*] Waiting for device to re-appear...
files\adb wait-for-device
echo.
setlocal
for /f "tokens=*" %%a in ('files\adb shell ls -l /system/xbin/su ^| findstr /C:"No such file or directory"') do @set suState=%%a
if "%suState%" == "" (
    endlocal
    set su=/system/xbin/su
    goto done
)
echo Something has deleted su - looking for the backup copy
for /f "tokens=*" %%a in ('files\adb shell ls -l /system/xbin/super ^| findstr /C:"No such file or directory"') do @set superState=%%a
if "%superState%" == "" (
    endlocal
    set su=/system/xbin/super
    goto done
) else (
    echo The backup is gone too - can't recover!
    goto exit
)

:done

echo | set /p dummy="Wait until your phone reboots,then unlock it and "
pause
echo On your phone, open SuperSU and let it update if it asks.
echo | set /p dummy="When SuperSU is done updating, "
pause
files\adb wait-for-device
echo On your phone, watch for the SuperSU permission popup and give
echo permission for ADB Shell to gain root permissions.
echo.
files\adb shell %su% -c sleep 1
files\adb wait-for-device
files\adb shell %su% -c sleep 1
files\adb wait-for-device
files\adb shell %su% -c sleep 1
files\adb wait-for-device
files\adb shell %su% -c sleep 1
files\adb wait-for-device
files\adb shell %su% -c sleep 1
files\adb wait-for-device
echo [*] Disabling Knox
files\adb shell %su% -c pm disable com.sec.knox.seandroid
echo [*] Setting Permissions
files\adb shell %su% -c mount -o remount,rw /system
files\adb shell %su% -c rm -f /system/etc/install-recovery-2.sh
files\adb shell %su% -c rm -f /system/xbin/selinuxoff
if "%su%" == "/system/xbin/super" (
    files\adb shell %su% -c cp /system/xbin/daemonsu /system/xbin/su
    files\adb shell %su% -c chown 0.0 /system/xbin/su
)
files\adb shell %su% -c chmod 6755 /system/xbin/su
files\adb shell %su% -c chmod 6755 /system/xbin/daemonsu
files\adb shell %su% -c chmod 6755 /system/xbin/super
files\adb shell su -c rm /system/xbin/super

if %BB%==Y (
    echo [*] Installing busybox
    files\adb wait-for-device
    files\adb shell %su% -c /system/xbin/busybox --install -s /system/xbin
    files\adb wait-for-device
)
files\adb shell sleep 15
files\adb wait-for-device
files\adb shell %su% -c mount -o remount,ro /system
echo.
echo --- All Finished ---
:exit
pause
