@echo off
cd "%~dp0"
set PATH=%SystemRoot%\system32;%PATH%
echo [*] Testing adb usability
echo.
echo | set /p dummy="Plug in your phone and "
pause
for /f "delims=" %%a in ('files\adb get-state ^| findstr /v "foobar"') do @set pstate=%%a
if not %pstate%==device (
    echo.
    echo Watch your phone. If you see the "Allow USB debugging?" prompt
    echo Tap on the "Always allow from this computer" checkbox
    echo then tap OK
    echo.
    echo If this script appears to be stuck at "Waiting for your phone to appear",
    echo then you should tap on the USB icon in the notifications that says
    echo "Connected as a media device". On the"USB computer connection"
    echo page, switch between "Camera" and "Media Device" to see if the
    echo device appears.
)
echo.
echo [*] Waiting for your phone to appear
files\adb wait-for-device
files\adb shell echo "[*] Your phone is detected and ready for unrooting."
echo.
files\adb shell su -c /system/xbin/unroot
echo.
echo --- All Finished ---
:exit
pause
