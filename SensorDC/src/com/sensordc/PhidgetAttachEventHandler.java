package com.sensordc;

import com.phidgets.Phidget;
import com.phidgets.PhidgetException;
import com.phidgets.TemperatureSensorPhidget;

class PhidgetAttachEventHandler implements Runnable { 
	Phidget device; 
	//TextView eventOutput;

	public PhidgetAttachEventHandler(Phidget device) {
		this.device = device;
	//	this.eventOutput = eventOutput;
	}

	public void run() {
		try {
		//	eventOutput.setText("Hello " + device.getDeviceName() + ", Serial " + Integer.toString(device.getSerialNumber())+" temp: "+((TemperatureSensorPhidget)device).getTemperature(0) );
			//if(device.getDeviceClass()==Phidget.PHIDCLASS_TEMPERATURESENSOR)
			((TemperatureSensorPhidget)device).setTemperatureChangeTrigger(0, 0);
			SensorDCLog.i("phidget attach event handler "," attached "+device.getDeviceName() , this.getClass());
			//	tempsensor.openAny();
			//	tempsensor.waitForAttachment();
			//TemperatureSensorPhidget tempsensor = (TemperatureSensorPhidget)device;
			//tempsensor.setTemperatureChangeTrigger(0, 0.1);
		} catch (PhidgetException e) {
		
			SensorDCLog.e("phidget attach event handler ",""+e, this.getClass());
//			Logger l = (Logger) LoggerFactory.getLogger(this.getClass());
//			l.error(e.toString());
			//eventOutput.setText(">>"+e);
		}
		catch(Exception e)
		{
			SensorDCLog.e("phidget attach event handler ",""+e, this.getClass());
//			Logger l = (Logger) LoggerFactory.getLogger(this.getClass());
//			l.error(e.toString());
			//eventOutput.setText(">>"+e);
		}
    
    	// Notify that we're done
//    	synchronized(this) {
//    		this.notify();
//    	}
	}
}
