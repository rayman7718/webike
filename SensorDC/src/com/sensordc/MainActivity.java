package com.sensordc;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity   {

	private static String TAG = "sensordcmainactivity";
	public static final String PREFS_NAME = "SensorDCPrefs";
	
	private static float T1AMBIENTDEFAULT = (float) 25.0;
	private static float T2AMBIENTDEFAULT = (float) 45.0;
	private static float V1AMBIENTDEFAULT = (float) 596.0;
	private static float V2AMBIENTDEFAULT = (float) 636.0;
	
	private static float T1BATTERYDEFAULT = (float) 25.0;
	private static float T2BATTERYDEFAULT = (float) 45.0;
	private static float V1BATTERYDEFAULT = (float) 596.0;
	private static float V2BATTERYDEFAULT = (float) 636.0;
	
	public static float t1ambient = T1AMBIENTDEFAULT ; 
	public static float t2ambient = T2AMBIENTDEFAULT  ;
	public static float v1ambient = V1AMBIENTDEFAULT ; 
	public static float v2ambient = V2AMBIENTDEFAULT  ;
	
	public static float t1battery = T1BATTERYDEFAULT ;
	public static float t2battery = T2BATTERYDEFAULT;
	public static float v1battery = V1BATTERYDEFAULT ;
	public static float v2battery = V2BATTERYDEFAULT;
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try
        {
        setContentView(R.layout.activity_main);

        Context context = this;
        Intent startServiceIntent = new Intent(context, SensorDCService.class);
        boolean retVal = context.stopService(startServiceIntent);
		SensorDCLog.i("MainActivity "," stopping service retVal: "+retVal, this.getClass());
		SensorDCLog.i("MainActivity "," starting service ", this.getClass());
		ComponentName ret = context.startService(startServiceIntent);
		if(ret==null)
		{
			SensorDCLog.e("MainActivity ","startService retVal: "+ret, this.getClass());
		}
		else
		{
			SensorDCLog.i("MainActivity ","startService retVal "+ret, this.getClass());
		}


        
      //  if(!isMyServiceRunning())
       // {
//        	Intent startServiceIntent = new Intent(getApplicationContext(), SensorDCService.class);
//        	ComponentName ret = startService(startServiceIntent);
//        	if(ret==null)
//        	{
//        		SensorDCLog.e(TAG,"startService="+ret, this.getClass());
//        	}
//        	
     //   }
        
        }
        catch(Exception e)
        {
        	SensorDCLog.i(TAG," main activity onCreate "+e, this.getClass());
        }
    }
    
    
    @Override
    protected void onResume() {
      super.onResume();
      Intent intent = getIntent();
      if (intent != null)
      {
    	  if(intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED))
    	  {
    		  finish();
    	  }
    	  
      }
      
      
      try 
      {
      SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
      float t1ambient = settings.getFloat("t1ambient",T1AMBIENTDEFAULT);
      float t2ambient = settings.getFloat("t2ambient",T2AMBIENTDEFAULT);
      float v1ambient = settings.getFloat("v1ambient",V1AMBIENTDEFAULT);
      float v2ambient = settings.getFloat("v2ambient",V2AMBIENTDEFAULT);
      
      float t1battery = settings.getFloat("t1battery",T1BATTERYDEFAULT);
      float t2battery = settings.getFloat("t2battery",T2BATTERYDEFAULT);
      float v1battery = settings.getFloat("v1battery",V1BATTERYDEFAULT);
      float v2battery = settings.getFloat("v2battery",V2BATTERYDEFAULT);
      
      
      final EditText t1a = (EditText)findViewById(R.id.t1ambient);
      final EditText t2a = (EditText)findViewById(R.id.t2ambient);
      final EditText v1a = (EditText)findViewById(R.id.v1ambient);
      final EditText v2a = (EditText)findViewById(R.id.v2ambient);
      
      final EditText t1b = (EditText)findViewById(R.id.t1battery);
      final EditText t2b = (EditText)findViewById(R.id.t2battery);
      final EditText v1b = (EditText)findViewById(R.id.v1battery);
      final EditText v2b = (EditText)findViewById(R.id.v2battery);
      
      t1a.setText(t1ambient+"");
      t2a.setText(t2ambient+"");
      v1a.setText(v1ambient+"");
      v2a.setText(v2ambient+"");
      
      t1b.setText(t1battery+"");
      t2b.setText(t2battery+"");
      v1b.setText(v1battery+"");
      v2b.setText(v2battery+"");
      
      MainActivity.t1ambient=t1ambient;
	  MainActivity.t2ambient=t2ambient;
	  MainActivity.v1ambient=v1ambient;
	  MainActivity.v2ambient=v2ambient;
	  
	  MainActivity.t1battery=t1battery;
	  MainActivity.t2battery=t2battery;
	  MainActivity.v1battery=v1battery;
	  MainActivity.v2battery=v2battery;
	  
      
      final Button button = (Button) findViewById(R.id.savebutton);
      button.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {
              
        	  float t1atemp = Float.parseFloat(t1a.getText().toString());
        	  float t2atemp = Float.parseFloat(t2a.getText().toString());
        	  float v1atemp = Float.parseFloat(v1a.getText().toString());
        	  float v2atemp = Float.parseFloat(v2a.getText().toString());
        	  
        	  float t1btemp = Float.parseFloat(t1b.getText().toString());
        	  float t2btemp = Float.parseFloat(t2b.getText().toString());
        	  float v1btemp = Float.parseFloat(v1b.getText().toString());
        	  float v2btemp = Float.parseFloat(v2b.getText().toString());
        	  
        	  SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        	  SharedPreferences.Editor editor = settings.edit();
        	  editor.putFloat("t1ambient",t1atemp);
        	  editor.putFloat("t2ambient",t2atemp);
        	  editor.putFloat("v1ambient",v1atemp);
        	  editor.putFloat("v2ambient",v2atemp);
        	  
        	  editor.putFloat("t1battery",t1btemp);
        	  editor.putFloat("t2battery",t2btemp);
        	  editor.putFloat("v1battery",v1btemp);
        	  editor.putFloat("v2battery",v2btemp);
        	  
        	  editor.commit();
        	  MainActivity.t1ambient=t1atemp;
        	  MainActivity.t2ambient=t2atemp;
        	  MainActivity.v1ambient=v1atemp;
        	  MainActivity.v2ambient=v2atemp;
        	  
        	  MainActivity.t1battery=t1btemp;
        	  MainActivity.t2battery=t2btemp;
        	  MainActivity.v1battery=v1btemp;
        	  MainActivity.v2battery=v2btemp;
        	  
        	  Toast toast = Toast.makeText(getApplicationContext(), "Saved. Good job!", Toast.LENGTH_SHORT);
        	  toast.show();
        	  
          }
      });
      
      
      }
      catch(Exception e)
      {
    	  SensorDCLog.i(TAG, "Main activity on resume: "+e , this.getClass());
      }
      
    }

    @Override
    protected void onPause() {
      super.onPause();
    }
    
    
         
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    /**
//     * A placeholder fragment containing a simple view.
//     */
//    public static class PlaceholderFragment extends Fragment {
//
//        public PlaceholderFragment() {
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//            return rootView;
//        }
//    }

}
