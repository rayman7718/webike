package com.sensordc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.ChannelSftp.LsEntry;

import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;




public class AsyncUpload extends AsyncTask<Void, Void, Void>    {

	
	
	public static String TAG = "sensordcasyncuploadtask";
	private String remotehost, remoteuser, remotedir, localdir;
	byte[] pubkey;
	byte[] privkey;
	
	private int remoteport;
	 PowerManager.WakeLock wl;
	
	AsyncUpload(String remotehost,String  remoteuser,byte[] pubkey, byte[] privkey,String  remotedir,String  localdir, int remoteport, PowerManager.WakeLock wl)    {
                this.remotehost=remotehost;
                this.remoteuser=remoteuser;
                //this.remotepassword=remotepassword;
                this.pubkey=pubkey;
                this.privkey=privkey;
                
                this.remotedir=remotedir;
                this.localdir=localdir;
                this.remoteport=remoteport;
                this.wl = wl;
                Log.i(TAG, "asynctask initialized to "+remotehost+","+remoteport+","+remoteuser+","+","+remotedir+","+localdir);
    }

    // Executed on the UI thread before the
    // time taking task begins
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        
    }

    // Executed on a special thread and all your
    // time taking tasks should be inside this method
    @Override
    protected Void doInBackground(Void... arg0) {
        
    	PerformUpload();
    	
        return null;
    }
    
    // Executed on the UI thread after the
    // time taking process is completed
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }
    

    
    
    private void PerformUpload() {

		//configureWifi();
		try {
			
			
			Log.i(TAG, "perform upload begins initialized to "+remotehost+","+remoteport+","+remoteuser+","+remotedir+","+localdir+","+remoteport);
			JSch ssh = new JSch();
			JSch.setConfig("StrictHostKeyChecking", "no");
			Session session = ssh.getSession(remoteuser, remotehost,
					remoteport);
			//session.setPassword(remotepassword);
			ssh.addIdentity(remoteuser, privkey, pubkey, null);
			
			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			
			ChannelSftp sftp = (ChannelSftp) channel;

			// make directory remote if it does not exist
			this.mkdir(sftp, remotedir);

			
			// list files in remote directory
			List<DataFileInfo> lsr = new ArrayList<DataFileInfo>();
			List<LsEntry> ls_remote = sftp.ls(remotedir);
			for (LsEntry l : ls_remote) {
				SftpATTRS  attrs = l.getAttrs();
				lsr.add(new DataFileInfo(l.getFilename(), attrs.getSize()));
			}
			
			
			channel.disconnect();
			session.disconnect();
			

			List<File> ls_local = ListDir(localdir);
			List<File> toUpload = new ArrayList<File>();

			// sync last log file regardless
		//	toUpload.add(new File(localdir + File.separator
			//		+ currentdatalogfilename));
			String currentlogfilename = SensorDCLog.getCurrentFileName();
			for (File f : ls_local) {
				DataFileInfo local = new DataFileInfo(f.getName(), f.length());
				if (!lsr.contains(local) && !f.getName().equals(currentlogfilename) ) {
					toUpload.add(f);
				}
			}
			
			
			session = ssh.getSession(remoteuser, remotehost,
					remoteport);
			//session.setPassword(remotepassword);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			sftp = (ChannelSftp) channel;
			
			
			for (File f : toUpload) {
				sftp.put(f.getAbsolutePath(), remotedir + "/" + f.getName() +".uploading");
				sftp.chmod(384, remotedir + "/" + f.getName()+".uploading");
				sftp.rename(remotedir + "/" + f.getName() +".uploading", remotedir + "/" + f.getName());
			}

			
			//Now lets delete things locally, keep 24 files as a buffer
			String currentfilename1 = SensorDCLog.getCurrentFileName();
			//special handling for gps-logger app. 
			String currentfilename2 = SensorDCLog.getCurrentFileName_GPSLogger();
			for (int i=0; i < ls_local.size();i++) {
				// dont delete files that are current being written to
				if(!ls_local.get(i).getName().equals(currentfilename1)
						&&!ls_local.get(i).getName().equals(currentfilename2))  
					ls_local.get(i).delete();
			}
			
			
			channel.disconnect();
			session.disconnect();
			Log.i(TAG, "upload completed to "+remotehost+","+remoteport+","+remoteuser+","+remotedir+","+localdir+","+remoteport);
		} catch (SftpException e) {
			SensorDCLog.e(TAG, "Datauploadalarm sftp exception " + e+e.getMessage(),
					this.getClass());
		} catch (Exception e) {
			SensorDCLog.e(TAG, "Datauploadalarm " + e+e.getMessage(), this.getClass());
		}
	}
	
	
	private List<File> ListDir(String path) {
		List<File> retVal = new ArrayList<File>();
		File f = new File(path);
		File[] files = f.listFiles();
		for (File file : files) {
			retVal.add(file);
		}
		
		return retVal;
	}
	
	private void mkdir(ChannelSftp sftp, String dir) {
		SftpATTRS attrs = null;
		try {
			String currentDirectory = sftp.pwd();
			attrs = sftp.stat(currentDirectory + "/" + dir);
		} catch (Exception e) {
			// do nothing
		}

		if (attrs != null) {
			return;
		} else {

			try {
				sftp.mkdir(dir);
			} catch (SftpException e) {
				SensorDCLog.e(TAG, " Datauploadalarm mkdir " + e,
						this.getClass());
			}
		}

	}


}