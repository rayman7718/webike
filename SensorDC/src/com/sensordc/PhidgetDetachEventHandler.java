package com.sensordc;

import com.phidgets.Phidget;

class PhidgetDetachEventHandler implements Runnable {
	Phidget device;
	//TextView eventOutput;
	
	public PhidgetDetachEventHandler(Phidget device) {
		this.device = device;
		//this.eventOutput = eventOutput;
	}
	
	public void run() {
		SensorDCLog.i("phidget detach event handler "," detached " , this.getClass());
//		try {
//		//	eventOutput.setText("Goodbye " + device.getDeviceName() + ", Serial " + Integer.toString(device.getSerialNumber()));
//		} catch (PhidgetException e) {
//			e.printStackTrace();
//		}
//    
    	// Notify that we're done
//    	synchronized(this) {
//    		this.notify();
//    	}
	}
}