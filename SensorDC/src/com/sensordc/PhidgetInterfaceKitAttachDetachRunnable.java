package com.sensordc;

import com.phidgets.Phidget;


class PhidgetInterfaceKitAttachDetachRunnable implements Runnable {
	Phidget phidget;
	boolean attach;
	public PhidgetInterfaceKitAttachDetachRunnable(Phidget phidget, boolean attach)
	{
		this.phidget = phidget;
		this.attach = attach;
	}
	public void run() {
		
        if(attach)
        {
        	try {
				SensorDCLog.i("phidget interface kit attach event handler "," attached "+phidget.getDeviceName() + " "+ phidget.getDeviceID() , this.getClass());
			} catch (Exception e) {
				
				SensorDCLog.i("phidget interface kit attach event handler "," attached "  , this.getClass());
			}
        	// do nothing here for now
        }
        else
        {
        	SensorDCLog.i("phidget interface kit detach event handler "," detached " , this.getClass());
        	
        }
        	// do nothing for now
    	//notify that we're done
//    	synchronized(this)
//    	{
//    		this.notify();
//    	}
    }
}
