package com.sensordc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.widget.Toast;

public class DataUploadAlarm extends BroadcastReceiver {
	

	private PowerManager.WakeLock wl;
	private static String TAG = "sensordcuploadalarm";
	
	private String remotehost, remoteuser, /*remotepassword,*/ deviceID;
	byte[] pubkey;
	byte[] privkey;
	
	private int remoteport;
	
	String remotedir;
	String localdir ;
	
	    
	@Override
	public void onReceive(Context context, Intent intent) {
		try 
		{
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
		wl.acquire();
		
		remotehost = intent.getExtras().getString("remotehost");
		remoteuser = intent.getExtras().getString("remoteuser");
		pubkey=intent.getExtras().getByteArray("pubkey");
		privkey=intent.getExtras().getByteArray("privkey");
		
		//remotepassword = intent.getExtras().getString("remotepassword");
		remoteport=intent.getExtras().getInt("remoteport");
		deviceID = intent.getExtras().getString("deviceID");
		
		
		localdir = SensorDCLog.DataLogDirectory;
		remotedir=deviceID;
		Toast.makeText(context, "trying upload", Toast.LENGTH_LONG).show();
		AsyncUpload upload = new AsyncUpload(remotehost, remoteuser, pubkey,privkey, remotedir, localdir, remoteport,wl);
		upload.execute();
		//upload.get();
		
		Toast.makeText(context, "upload done", Toast.LENGTH_LONG).show();
		wl.release();
		}
		catch(Exception e)
		{
			SensorDCLog.e(TAG, "Exception in dataupload alarm "+e+e.getMessage(), this.getClass());
		}
		}
	
		



}
